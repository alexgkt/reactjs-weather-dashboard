## File Structure
```
📦src
 ┣ 📂component
 ┃ ┣ 📜AddCity.js - Component to add city to dashboard
 ┃ ┣ 📜Cities.js - Component that list all added cities on dashboard
 ┃ ┣ 📜City.js - Component of individual city on the dashboard 
 ┃ ┗ 📜SearchCities.js - Component that search & autocomplete a list of countries 
 ┣ 📂data
 ┃ ┗ 📜city.list.json - Cities JSON data file provided by OpenWeather
 ┣ 📂service
 ┃ ┣ 📜City.js - Service to call cities for autocomplete
 ┃ ┗ 📜Weather.js - Service to call OpenWeather API
 ┣ 📜.env.example - Copy to `.env` and key in the API key from OpenWeather
 ┣ 📜App.css - Default ReactJS file 
 ┣ 📜App.js - App main component
 ┣ 📜App.test.js - Default ReactJS file 
 ┣ 📜Interval.js - Polling library file
 ┣ 📜Store.js - State management using `useReducer` hook
 ┣ 📜index.css - Default ReactJS file 
 ┣ 📜index.js - App bootstrap js file
 ┣ 📜logo.svg - Default ReactJS file 
 ┣ 📜serviceWorker.js - Default ReactJS file 
 ┗ 📜setupTests.js - Default ReactJS file 
```
 ## Instructions

 1. `yarn install` to install all depedencies.
 2. Copy `.env.example` file to `.env`.
 2. Register to obtain API key from OpenWeather and add it into `.env` file.
 3. `yarn start` to launch app.
 4. Search for cities through the autocomplete textfield.
 5. Add the `+` button.
 6. Set the duration in seconds then click `+` button to add to dashboard.
 7. Add as many cities as needed.
 8. Wait for the card to refresh with new data based on the set refresh time.
 9. Can open developer tool's console to check out the polling features.
