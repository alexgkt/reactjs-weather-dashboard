import React from 'react';
import Container from '@material-ui/core/Container';
import Box from '@material-ui/core/Box';
import Store from './Store';
import Cities from './component/Cities';
import SearchCities from "./component/SearchCities";
import AddCity from "./component/AddCity";
//import './App.css';


function App() {
  return (
    <Container className="App">
      <Box>
        <header className="App-header"></header>
        <Store>
          <Cities />
          <SearchCities />
          <AddCity />
        </Store>
      </Box>
    </Container>
  );
}

export default App;
