import React, {createContext, useReducer} from "react";

const initialState = {
    selected: {},
    cities: [] 
};

// Reducer to map state to action
const reducer = (state, action) => {
    let cities = state.cities;
    switch (action.type) {
      // Adding the selected city
      case "SELECT_CITY":
        return Object.assign({}, state, { selected: action.payload });
      // Removing the selected city
      case "UNSELECT_CITY":
        return Object.assign({}, state, { selected: {} });
      // Add city id and refresh rate to cities array
      case "PUSH_CITY":
        cities.push(action.payload);
        return Object.assign({}, state, { selected: {} }, { cities: cities });
      // Remove city id and refresh rate from cities array
      case "POP_CITY":
        cities.splice(action.payload, 1);
        return Object.assign({}, state, {
          cities: cities,
        });
      default:
        return state;
    }
}

// The Storge
const Store = ({ children }) => {
    const [state, dispatch] = useReducer(reducer, initialState);

    return (
        <Context.Provider value={[state, dispatch]} >
            {children}
        </Context.Provider>
    );
}

export const Context = createContext(initialState);

export default Store;
