import Cities from "./../data/city.list.json";

export default {
    // filter cities list by name
    find(q) {
        q = q || '';
        let results = [];
        if (q.length > 3) {
            Cities.forEach((data) => {
                if (data.name.search(/q/i)) {
                    results.push({
                        id: data.id,
                        name: data.name,
                    })
                }
            });
        }
        return results;
    }
}