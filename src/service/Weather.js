const apiUrl = "https://api.openweathermap.org/data/2.5/";
const apiKey = process.env.REACT_APP_OWM_API_KEY;

export default {
    // Retrieve weather data based on city id
    async id(q) {
        const endpoint = apiUrl + 'weather?id=' + q + '&units=metric&appid=' + apiKey;
        const response = await fetch(endpoint);
        const data = await response.json();
        return data;
    }
}