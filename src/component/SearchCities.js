import React, { useEffect, useContext, useCallback } from "react";
import { makeStyles } from "@material-ui/core/styles";
import TextField from "@material-ui/core/TextField";
import Autocomplete from "@material-ui/lab/Autocomplete";
import CircularProgress from "@material-ui/core/CircularProgress";
import ServiceCity from "../service/City";
import { Context } from "../Store";

const useStyles = makeStyles({
  root: {
    marginTop: 100,
    width: '100%',
    textAlign: 'center'
  },
  textfield: {
    maxWidth: 600,
    width: '100%'
  }
});

export default function SearchCities() {
  const classes = useStyles();
  const [state, dispatch] = useContext(Context);
  const [open, setOpen] = React.useState(false);
  const [options, setOptions] = React.useState([]);
  const loading = open && options.length === 0;
  
  // Callback to filter city list on input change
  const searchCity = useCallback((event, value) => {
    value = value || '';
    if (value.length > 3) {
      setOptions(ServiceCity.find(value));
    }
  }, []);

  // Callback to push selected city to storage state
  const selectCity = useCallback((event, value) => {
    dispatch({ type: 'SELECT_CITY', payload: value});
  },[dispatch]);

  // useEffect hook when autocomplete is loading
  useEffect(() => {
    let active = true;

    if (!loading) {
      return undefined;
    }

    return () => {
      active = false;
    };
  }, [loading]);

  // useEffect hook to empty options list on close
  useEffect(() => {
    if (!open) {
      setOptions([]);
    }
  }, [open]);

  return (
    <Autocomplete
      className={classes.root}
      id="cities"
      open={open}
      onOpen={() => {
        setOpen(true);
      }}
      onClose={() => {
        setOpen(false);
      }}
      onChange={selectCity}
      onInputChange={searchCity}
      getOptionSelected={(option, value) => option.name === value.name}
      getOptionLabel={(option) => option.name}
      options={options}
      loading={loading}
      renderInput={(params) => (
        <TextField
          {...params}
          className={classes.textfield}
          label="Cities"
          variant="outlined"
          InputProps={{
            ...params.InputProps,
            endAdornment: (
              <React.Fragment>
                {loading ? (
                  <CircularProgress color="inherit" size={20} />
                ) : null}
                {params.InputProps.endAdornment}
              </React.Fragment>
            ),
          }}
        />
      )}
    />
  );
}