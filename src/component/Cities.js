import React, { useEffect, useContext } from "react";
import Grid from "@material-ui/core/Grid";
import City from "../component/City";
import { Context } from "../Store";

export default function Cities() {
  const [state, dispatch] = useContext(Context);
  const [cities, setCities] = React.useState('');

  useEffect(() => {
    // retrieve cities id and refresh rate to pass to City component
    let cities = state.cities.map((row, i) => {
      return (
        <City id={row.id} refresh={row.refresh} i={i} key={i}/>
      );
    });
    // assign to state to be return to render
    setCities(cities);
  }, [state]);

  return (
    <Grid container spacing={3}>
      {cities}
    </Grid>
  );
}
