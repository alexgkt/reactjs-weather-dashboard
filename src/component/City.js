import React, { useState, useEffect, useContext, useCallback } from "react";
import { makeStyles } from "@material-ui/core/styles";
import Grid from "@material-ui/core/Grid";
import Card from "@material-ui/core/Card";
import CardHeader from "@material-ui/core/CardHeader";
import CardMedia from "@material-ui/core/CardMedia";
import CardContent from "@material-ui/core/CardContent";
import IconButton from "@material-ui/core/IconButton";
import Typography from "@material-ui/core/Typography";
import RemoveIcon from "@material-ui/icons/RemoveCircle";
import { Context } from "../Store";
import { useInterval } from "../Interval";
import ServiceWeather from "./../service/Weather";

const useStyles = makeStyles({
  root: {
    minHeight: 100,
    maxWidth: 345,
    height: "100%",
  },
  media: {
    height: 140,
  },
});

export default function City(props) {
    const classes = useStyles();
    const [state, dispatch] = useContext(Context);
    const [name, setName] = useState('');
    const [weather, setWeather] = useState({
        main: '',
        icon: '',
        description: '',
    });
    const [icon, setIcon] = useState('');
    const [main, setMain] = useState({
        feels_like: '',
        humidity: '',
        presure: '',
        temp: '',
        temp_max: '',
        temp_min: ''
    });

    // retrieve city data from weather api
    const getCity = useCallback(() => {
        (async () => {
            let city = await ServiceWeather.id(props.id);
            setName(city.name);
            setWeather(city.weather[0]);
            setIcon("http://openweathermap.org/img/wn/" + city.weather[0].icon + "@4x.png");
            setMain(city.main);
        })();
    },[props.id])

    // Get gity data on dom load
    useEffect(() => {
        getCity();
    }, [getCity])
    
    // Remove city from Storage state
    const removeCity = (i) => {
        dispatch({ type: "POP_CITY", payload: i });
    };

    // Polling executed every 1sec
    let counter = 0;
    useInterval(async () => {
        counter = counter + 1;
        // execute when the counter matched the refresh rate
        if (counter % props.refresh === 0) {
            getCity();
            console.log(name + " refreshed at " + counter + "s");
        }
    }, 1000);

    return (
        <Grid item xs={12} sm={4} key={props.i}>
            <Card className={classes.root}>
            <CardHeader
                action={
                <IconButton
                    className={classes.mouseOver}
                    onClick={() => removeCity(props.i)}
                >
                    <RemoveIcon />
                </IconButton>
                }
                title={name}
                subheader={weather.main}
            />
            <CardMedia
                className={classes.media}
                image={icon}
                title={weather.description}
            />
            <CardContent>
                <Typography
                variant="body2"
                color="textSecondary"
                component="div"
                >
                <div>
                    <strong>Feels Like: </strong>
                    <em>{main.feels_like}&#8451;</em>
                </div>
                <div>
                    <strong>Humidity: </strong>
                    <em>{main.humidity}</em>
                </div>
                <div>
                    <strong>Pressure: </strong>
                    <em>{main.pressure}</em>
                </div>
                <div>
                    <strong>Temperature: </strong>
                    <em>{main.temp}&#8451;</em>
                </div>
                <div>
                    <strong>Temp (Max): </strong>
                    <em>{main.temp_max}&#8451;</em>
                </div>
                <div>
                    <strong>Temp (Min): </strong>
                    <em>{main.temp_min}&#8451;</em>
                </div>
                </Typography>
            </CardContent>
            </Card>
        </Grid>
    );
}