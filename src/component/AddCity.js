import React, { useEffect, useContext } from "react";
import { makeStyles } from "@material-ui/core/styles";
import Box from "@material-ui/core/Box";
import Modal from "@material-ui/core/Modal";
import TextField from "@material-ui/core/TextField";
import InputAdornment from "@material-ui/core/InputAdornment";
import FormHelperText from "@material-ui/core/FormHelperText";
import IconButton from "@material-ui/core/IconButton";
import AddIcon from "@material-ui/icons/AddCircle";
import { Context } from "../Store";

function getModalStyle() {
  const top = 50;
  const left = 50;

  return {
    top: `${top}%`,
    left: `${left}%`,
    transform: `translate(-${top}%, -${left}%)`,
  };
}

const useStyles = makeStyles((theme) => ({
    box: {
        width: '100%',
        textAlign: 'center',
    },
    paper: {
        position: "absolute",
        width: 400,
        backgroundColor: theme.palette.background.paper,
        border: "2px solid #000",
        boxShadow: theme.shadows[5],
        padding: theme.spacing(2, 4, 3),
    },
}));

export default function SimpleModal() {
    const [state, dispatch] = useContext(Context);
    const classes = useStyles();
    // getModalStyle is not a pure function, we roll the style only on the first render
    const [modalStyle] = React.useState(getModalStyle);
    const [open, setOpen] = React.useState(false);
    const [visible, setVisible] = React.useState({display: 'none'});
    const [refresh, setRefresh] = React.useState(60)

    // handle modal open event
    const handleOpen = () => {
        setOpen(true);
    };

    // handle modal close event
    const handleClose = () => {
        setOpen(false);
    };

    // handle input value change event
    const handleInput = (event) => {
        setRefresh(event.target.value);
    }

    // handle + button submit even
    const addCity = () => {
      // Push added city details to Storage state
      dispatch({ 
        type: "PUSH_CITY", 
        payload: {
          id: state.selected.id, 
          refresh: refresh
        }
      });
      setOpen(false);
    }

    // Modal body content
    const body = (
      <div style={modalStyle} className={classes.paper}>
        <form className={classes.root} noValidate autoComplete="off">
          <TextField
            id="outlined-basic"
            label="Refresh Rate"
            variant="outlined"
            onChange={handleInput}
            defaultValue={refresh}
            InputProps={{
              endAdornment: <InputAdornment position="end">sec</InputAdornment>,
            }}
          />
          <FormHelperText id="standard-seconds-helper-text">
            Seconds
          </FormHelperText>
          <IconButton onClick={addCity}>
            <AddIcon />
          </IconButton>
        </form>
      </div>
    );
    
    useEffect(() => {
        const selected = state.selected || {};
        if (Object.keys(selected).length > 0) {
            setVisible({});
        }
    }, [state, state.selected]);

    return (
        <Box style={visible} className={classes.box}>
            <IconButton onClick={handleOpen}>
                <AddIcon />
            </IconButton>
            <Modal
                open={open}
                onClose={handleClose}
                aria-labelledby="simple-modal-title"
                aria-describedby="simple-modal-description"
            >
                {body}
            </Modal>
        </Box>
    );
}
